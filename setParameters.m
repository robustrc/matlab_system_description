%% DEFINE VEHICLE PARAMETERS

PARAMETER.m   =  ureal('m',0.207,'percent', eps);   %    Vehicle mass. [kg]
PARAMETER.a   =  ureal('a',0.0495,'percent',eps);   %    Distance from front axle to COG. [m]
PARAMETER.L   =  0.102;           %    Distance between rear and front axle. [m]
PARAMETER.b   =  PARAMETER.L-PARAMETER.a;    %    Distance from rear axle to COG. [m]
PARAMETER.mu = 0.5;                                 % Friction coefficient
PARAMETER.Iz  = (1/12)*PARAMETER.m*((PARAMETER.L*1.1)^2 + 0.06^2);       %  Rotational inertia Z axis.
PARAMETER.Iz.Name = 'Iz';
PARAMETER.rfw = 0.022/2;                            % Radius front wheel [m]
PARAMETER.rrw = 0.027/2;                            % Radius rear wheel [m]
PARAMETER.mrw = 0.003;                              % Mass rear wheel
PARAMETER.mfw = 0.0025;                             % Mass front wheel
PARAMETER.Iwr = (1/2)*PARAMETER.mrw^2;              % Rotational inertia rear wheel.
PARAMETER.Iwf = (1/2)*PARAMETER.mfw^2;              % Rotational inertia front wheel.

 % Lateral tire stiffness.  [NO FUCKING CLUE]
Cyr = PARAMETER.m.NominalValue*31;
PARAMETER.Cyr = ureal('Cyr',Cyr,'percent',eps);
PARAMETER.stiffnessRatio = ureal('stiffnessRatio',1.04,'percent',eps);
PARAMETER.Cyf = Cyr*PARAMETER.stiffnessRatio;
PARAMETER.Cyf.Name = 'Cyf'
PARAMETER.Ck = (1-(PARAMETER.a/PARAMETER.L))*PARAMETER.m  / (2*PARAMETER.Cyf*25);        % Steering angle to slip ratio factor (super made up)
PARAMETER.Cd  = 0.8;                                % Air resistance coefficient. [Unitless (0,1)]
PARAMETER.p   = 1.184;                              % Density of air [kg/dm^3]
PARAMETER.Af  = 0.002175;                           % Frontal areal of vehicle [m^2]
PARAMETER.Vx_lin = ureal('Vx',2,'percent',eps);   % Speed at which we linearize around [m/s]