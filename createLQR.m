function K_error = createLQR(car_model)
% CREATE CONTROLLER
% LQR controller matrices
Q = [
      1   0     0   0   0   0
      0   1     0   0   0   0
      0   0     1   0   0   0
      0   0     0   1   0   0
      0   0     0   0   1e2 0
      0   0     0   0   0   1e3
    ];

R = [100  0
    0   1];

[K_error,~,~] = lqr(car_model,Q,R);
K_error = [0 , 0 , 0 , 0 , 0,K_error(1,6);K_error(2,:)];