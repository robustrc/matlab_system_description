function [linear_car_model, error_based_linear_car_model] = ...
    createModels(PARAMETER)
% A bit messy but increases readability of the equations.
m   = PARAMETER.m;
a   = PARAMETER.a;
b   = PARAMETER.b;
L   = PARAMETER.L;
mu  = PARAMETER.mu;
Iz  = PARAMETER.Iz;
rfw = PARAMETER.rfw;
rrw = PARAMETER.rrw;
mrw = PARAMETER.mrw;
mfw = PARAMETER.mfw;
Iwr = PARAMETER.Iwr;
Iwf = PARAMETER.Iwf;
Cyr  = PARAMETER.Cyr;
Cyf  = PARAMETER.Cyf; 
Ck  = PARAMETER.Ck;
Cd  = PARAMETER.Cd;
p   = PARAMETER.p;
Af  = PARAMETER.Af;
Vx_lin = PARAMETER.Vx_lin;

% A matrix for state vector [x xdot y ydot yaw yawdot]
A = [
       0   0   0                               0   0
       0   0   1                               0   0
       0   0   -(2*Cyf+2*Cyr)/(Vx_lin*m)       0   -Vx_lin*Ck-((2*Cyf*a-2*Cyr*b)/(Vx_lin*m))
       0   0   0                               0    1
       0   0   -(2*Cyf*a-2*Cyr*b)/(Iz*Vx_lin)  0   -(2*(a^2)*Cyf+2*(b^2)*Cyr)/(Iz*Vx_lin)
    ];

% A matrix for state vector [e0 e0Dot e1 e1dot e2 e2dot]
A_error = [
           0    0   0   0                               0                       0
           0    0   1   0                               0                       0
           0    0   0   1                               0                       0
           0    0   0    -(2*Cyf+2*Cyr)/(Vx_lin*m)     (2*Cyf+2*Cyr)/m         (-2*Cyf*a+2*Cyr*b)/(Vx_lin*m)
           0    0   0    0                               0                       1
           0    0   0    -(2*Cyf*a-2*Cyr*b)/(Iz*Vx_lin)  (2*Cyf*a-2*Cyr*b)/Iz    -(2*(a^2)*Cyf+2*(b^2)*Cyr)/(Iz*Vx_lin)
    ];

% B matrix for state vector [x xdot y ydot yaw yawdot]
B = [
    0               1/(rrw*2*m)
    0               0
    2*Ck*Cyf/m      0
    0               0
    (2*Cyf*a/Iz)    0
    ];

% B matrix for state vector [e1 e1dot e2 e2dot]
B_error_ctrl = [
    0               1/(rrw*2*m)
    0               0
    0               0
    2*Ck*Cyf/m      0
    0               0
    (2*Cyf*a/Iz)    0
    ];
B_error_ref = [
    0
    0
    (-(2*Cyf*a-2*Cyr*b)/(m*Vx_lin)-Vx_lin)
    0
    -(2*Cyf*a^2+2*Cyr*b^2)/(Iz*Vx_lin)
    ];

C = eye(5);
C_error = eye(6);

D = zeros(5,2);
D_error = zeros(6,2);





% Create the state space models with proper names and stuff
linear_car_model = ss(A,B,C,D);
error_based_linear_car_model = ss(A_error, ...
    B_error_ctrl, C_error, D_error);

error_based_linear_car_model.InputName = {'\delta','torque'};
error_based_linear_car_model.OutputName = {'V_e','e_1_{Int}','e_1','\dot{e}_1','e_2','\dot{e}_2'};
