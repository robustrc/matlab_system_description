clear
clf
close all
clc
% CREATE A TRACK
% Create a track by left clicking the graph to place way points in the
% same order as 'clicked'. Finish by right clicking.
run('createTrack');
csvwrite('track.csv',track');
save('data');
%%
% To specify parameters change valaues within setParameters.m
run('setParameters');
nominalParameters = getNominalParameters(PARAMETER);

% Note that the created car models are uncertain
[linear_car_model, error_based_linear_car_model] = createModels(PARAMETER);
% Nominal value used since LQR design does not support uncertain models.
K_error = createLQR(error_based_linear_car_model.NominalValue);

% Exporting the controller enables it to be imported for real world testing
dlmwrite('controller.csv',K_error);
dlmwrite('parameters.csv',nominalParameters);
%% RUN SIMULATIONS
% Simulation parameters.
clc
clf
close all

dt = 1/400;
plotStep = 50;

% The h-inf controller is created within it's own script where relevant
% system description is described.
[Chinf, CL, GammaHinf, info, A, B_2] = createHinf(PARAMETER);
[ah,bh,ch,dh] = ssdata(Chinf);
% Exporting the h-inf controller (Note that the frequency should match the 
% frequency of the implemented controller)
[ahd,bhd,chd,dhd] = ssdata(c2d(Chinf, dt));
dlmwrite('ahd.csv',ahd);
dlmwrite('bhd.csv',bhd);
dlmwrite('chd.csv',chd);
dlmwrite('dhd.csv',dhd);

% The for loop is activated for analysis of velocites that differ from the
% ones used to construct the controller 

%for i=1:1 
% PARAMETER.Vx_lin = ureal('Vx',i,'percent',eps);   % Speed at which we linearize around [m/s]

w = warning ('off','all');
sim_init_linear_model = [PARAMETER.Vx_lin.NominalValue,0,0,track(3,1),0]; %[xdot y ydot yaw yawdot]
sim_init_error_model = [track(1,1),PARAMETER.Vx_lin.NominalValue,track(2,1),0,track(3,1),0];
carInitialPos = [track(1,1),track(2,1)];
sim_time = 10;

save('data');

runSimulations(carInitialPos,dt,plotStep);
load('results.mat')

 run('RadiusAnalysis')
%end


%% ANALYSIS
% Script snippet left.
% constructs the feedback system of the lqr controller with desired yaw
% rate as an input.

clc
A = error_based_linear_car_model.A.NominalValue;
A = A(end-3:end,end-3:end);
B_1 = error_based_linear_car_model.B.NominalValue;
B_1 = B_1(end-3:end,1);
K = K_error(1,end-3:end);

B_2 = [
    0
    (-(2*PARAMETER.Cyf*PARAMETER.a-2*PARAMETER.Cyr*PARAMETER.b)/(PARAMETER.m*PARAMETER.Vx_lin)-PARAMETER.Vx_lin)
    0
    -(2*PARAMETER.Cyf*PARAMETER.a^2+2*PARAMETER.Cyr*PARAMETER.b^2)/(PARAMETER.Iz*PARAMETER.Vx_lin)
    ];

s = tf('s');
SteerDelay = [1/(s/2 +1)];

S = (1/s)*eye(4); %intergrator
systemnames = 'B_1 B_2 A SteerDelay K S'; 
inputvar = '[des_yawrate]';
input_to_B_2 = '[des_yawrate]';
outputvar = '[S]';
input_to_SteerDelay = '[-K]';
input_to_S = '[B_1 + B_2 + A]';
input_to_A = '[S]';
input_to_K = '[S]';
input_to_B_1 = '[SteerDelay]';


cleanupsysic = 'yes';
T = sysic; 
exist('inputvar') 
