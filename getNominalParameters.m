function nominalParameters = getNominalParameters(PARAMETER)
    nominalParameters = [0];
    fn = fieldnames(PARAMETER);
    for k=1:numel(fn)
        if( isuncertain(PARAMETER.(fn{k})) )
            nominalParameters(k) = PARAMETER.(fn{k}).NominalValue;
        else
            nominalParameters(k) = PARAMETER.(fn{k});
        end
    end