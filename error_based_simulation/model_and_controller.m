%% CREATE VEHICLE MODEL
clear
clc
clf
close all

run('../setParameters.m');
m   = PARAMETER.m;
a   = PARAMETER.a;
b   = PARAMETER.b;
L   = PARAMETER.L;
mu  = PARAMETER.mu;
Iz  = PARAMETER.Iz;
rfw = PARAMETER.rfw;
rrw = PARAMETER.rrw;
mrw = PARAMETER.mrw;
mfw = PARAMETER.mfw;
Iwr = PARAMETER.Iwr;
Iwf = PARAMETER.Iwf;
Cyr  = PARAMETER.Cyr;
Cyf  = PARAMETER.Cyf; 
Ck  = PARAMETER.Ck;
Cd  = PARAMETER.Cd;
p   = PARAMETER.p;
Af  = PARAMETER.Af;
Vx_lin = PARAMETER.Vx_lin;
delay = 20;       % delay for steering actuator

% A matrix for state vector [e1 e1dot e2 e2dot]
A = [
           0   1                               0                       0
           0    -(2*Cyf+2*Cyr)/(Vx_lin*m)     (2*Cyf+2*Cyr)/m         -((2*Cyf*a-2*Cyr*b))/(Vx_lin*m)
           0    0                               0                       1
           0    -(2*Cyf*a-2*Cyr*b)/(Iz*Vx_lin)  (2*Cyf*a-2*Cyr*b)/Iz    -(2*(a^2)*Cyf+2*(b^2)*Cyr)/(Iz*Vx_lin)
    ];
A_aug = [0 1 0 0 0;
        [zeros(4,1) A]];

% B matrix for state vector [e1 e1dot e2 e2dot]
B_1 = [
    0               
    2*Ck*Cyf/m      
    0               
    (2*Cyf*a/Iz)    
    ];
B_1_aug = [0; B_1];

B_2 = [
    0
    (-(2*Cyf*a-2*Cyr*b)/(m*Vx_lin)-Vx_lin)
    0
    -(2*Cyf*a^2+2*Cyr*b^2)/(Iz*Vx_lin)
    ];
B_2_aug = [0; B_2];

B = [B_2,B_1];

B_aug = [B_2_aug, B_1_aug];

C = eye(4);

C_aug = eye(5);

D = zeros(4,1);

D_aug = zeros(5,1);


% Create the state space models with proper names and stuff
error_based_linear_car_model = ss(A, ...
    B_1, C, D);

error_based_linear_car_model_aug = ss(A_aug, ...
    B_1_aug, C_aug, D_aug);

complete_sys = ss(A,B,C,[D,D]);

error_based_linear_car_model.InputName = {'\delta'};
error_based_linear_car_model.OutputName = {'e_1','\dot{e}_1','e_2','\dot{e}_2'};

% CREATE CONTROLLER
% LQR controller matrices
Q = diag([1 1 1 1]);
Q_aug = diag([1,1,1,1,1]);
R = [1];
R_aug = 1;
[K,~,~] = lqr(error_based_linear_car_model.NominalValue,Q,R);
[K_aug,~,~] = lqr(error_based_linear_car_model_aug.NominalValue,Q_aug,R_aug);
K_2input = [zeros(1,4);K];

F = loopsens(error_based_linear_car_model_aug, K_aug);


dt = 1e-3;

%[wcg,wcu,info] = wcgain(F.PSi)
%%
clc

s = tf('s');
SteerDelay = 1/(s/delay +1);

S = (1/s)*eye(4); %intergrator
systemnames = 'B_1 B_2 A SteerDelay K S'; 
inputvar = '[des_yawrate]';
input_to_B_2 = '[des_yawrate]';
outputvar = '[S]';
input_to_SteerDelay = '[-K]';
input_to_S = '[B_1 + B_2 + A]';
input_to_A = '[S]';
input_to_K = '[S]';
input_to_B_1 = '[SteerDelay]';


cleanupsysic = 'yes';
T = sysic; 
exist('inputvar') 
