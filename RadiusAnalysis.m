%% TRACK RADIUS ANALYSIS

sampleRate = 200;
trackRadius = 2./simout1.data;
Y=fft(trackRadius,length(trackRadius));

Pyy = Y.*conj(Y)/length(trackRadius);
f = sampleRate/length(trackRadius)*(0:length(trackRadius)/2-1);
figure(21)
loglog(f,Pyy(1:length(trackRadius)/2))
hold on
title('Power spectral density')
xlabel('Frequency (Hz)')