function [Chinf, CL, GammaHinf, info, A, B_2] = createHinf(PARAMETER)
% CREATE VEHICLE MODEL
m    = PARAMETER.m;
a    = PARAMETER.a;
b    = PARAMETER.b;
L    = PARAMETER.L;
mu   = PARAMETER.mu;
Iz   = PARAMETER.Iz;
rfw  = PARAMETER.rfw;
rrw  = PARAMETER.rrw;
mrw  = PARAMETER.mrw;
mfw  = PARAMETER.mfw;
Iwr  = PARAMETER.Iwr;
Iwf  = PARAMETER.Iwf;
Cyr  = PARAMETER.Cyr;
Cyf  = PARAMETER.Cyf; 
Ck   = PARAMETER.Ck;
Cd   = PARAMETER.Cd;
p    = PARAMETER.p;
Af   = PARAMETER.Af;
Vx_lin = PARAMETER.Vx_lin;
   

% A matrix for state vector [e1 e1dot e2 e2dot]
A = [
           0   1                               0                       0
           0    -(2*Cyf+2*Cyr)/(Vx_lin*m)     (2*Cyf+2*Cyr)/m         -((2*Cyf*a-2*Cyr*b))/(Vx_lin*m)
           0    0                               0                       1
           0    -(2*Cyf*a-2*Cyr*b)/(Iz*Vx_lin)  (2*Cyf*a-2*Cyr*b)/Iz    -(2*(a^2)*Cyf+2*(b^2)*Cyr)/(Iz*Vx_lin)
    ];

% B matrix for state vector [e1 e1dot e2 e2dot]
B_1 = [
    0               
    2*Ck*Cyf/m      
    0               
    (2*Cyf*a/Iz)    
    ];

B_2 = [
    0
    (-(2*Cyf*a-2*Cyr*b)/(m*Vx_lin)-Vx_lin)
    0
    -(2*Cyf*a^2+2*Cyr*b^2)/(Iz*Vx_lin)
    ];

C = eye(4);
D = zeros(4,1);

s = tf('s');
Steer_Delay = ss((s/100+5)/(s +5));

% Weighting of input signal desired yawrate. Treated as a disturbence
W_phiDot_des = makeweight(3000,100/(2*pi),0.001);

% Weighting of output from B_1 (delta to (error) states)
W_B_1 = B_1*(makeweight(1.1,1,0.1)*ones(1,4));

% Weighting of desired yawrate to (error) states
W_B_2 = B_2*(makeweight(1.1,1,0.1)*ones(1,4));

% Weighting of measurment noise
w_n_1 = 0.1;
w_n_2 = 0.2;
w_n_3 = 0.09*pi/180;
w_n_4 = 0.15*pi/180;
W_N = blkdiag(w_n_1, w_n_2, w_n_3, w_n_4);

% Weighting of performence output z
w_perf_yaw = makeweight(5,0.5,0.1);
w_perf_yaw_rate = makeweight(10,1,0.1);
W_PERF = blkdiag(w_perf_yaw, w_perf_yaw_rate);

% Weighting of state.
w_s = makeweight(0.1,1,1.1);
W_S = blkdiag(w_s, w_s, w_s, w_s);

S = blkdiag((1/s),(1/s),(1/s),(1/s)); %intergrator
S = ss(S);

%--------------------------------------------------------------------------
if false
sys = uss(A,B,C,D);

systemnames = 'idealPlant W_phiDot_des Steer_Delay sys W_S W_N W_PERF'; 
inputvar = '[unc1{4}; des_yawrate{1}; noise{4}; delta{1}]';
outputvar = '[W_S; W_PERF; unc1+sys+W_N; des_yawrate]';

input_to_idealPlant = '[des_yawrate]';
input_to_Steer_Delay = '[delta]';
input_to_sys = '[Steer_Delay]';
input_to_W_S = '[sys]';
input_to_W_PERF = '[sys(4)-idealPlant]';
input_to_W_N = '[noise]';


cleanupsysic = 'yes';
T = sysic; 

nmeas = 4;
ncont = 2;
[Chinf, CL, GammaHinf, info] = dksyn(T,nmeas,ncont);
end
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
if true
systemnames = 'W_phiDot_des W_B_1 W_B_2 Steer_Delay B_1 B_2 A S W_S W_N W_PERF'; 
inputvar = '[unc1{4};unc2{4};unc3{4}; des_yawrate; noise{4}; delta{1}]';
outputvar = '[W_B_1; W_B_2; W_S; W_PERF; unc3+S+W_N]';

input_to_W_phiDot_des = '[des_yawrate]';
input_to_B_2 = '[W_phiDot_des]';
input_to_W_B_2 = '[B_2]';
input_to_Steer_Delay = '[delta]';
input_to_B_1 = '[Steer_Delay]';
input_to_W_B_1 = '[B_1]';
input_to_S = '[B_1 + unc1 + B_2 + unc2 + A]';
input_to_W_S = '[S]';
input_to_A = '[S]';
input_to_W_PERF = '[S(3);S(4)]';
input_to_W_N = '[noise]';

systemnames = 'W_phiDot_des Steer_Delay B_1 W_B_2 B_2 A S W_S W_N W_PERF'; 
inputvar = '[unc2{4}; unc3{4}; des_yawrate; noise{4}; delta{1}]';
outputvar = '[W_B_2; W_S; W_PERF; unc3+S+W_N]';

input_to_W_phiDot_des = '[des_yawrate]';
input_to_B_2 = '[W_phiDot_des]';
input_to_W_B_2 = '[B_2]';
input_to_Steer_Delay = '[delta]';
input_to_B_1 = '[Steer_Delay]';
%input_to_W_B_1 = '[B_1]';
input_to_S = '[B_1 + unc2 + B_2 + A]';
input_to_W_S = '[S]';
input_to_A = '[S]';
input_to_W_PERF = '[S(3);S(4)]';
input_to_W_N = '[noise]';


cleanupsysic = 'yes';
T = sysic; 

hold on
sigma(T,'g')

nmeas = 4;
ncont = 1;
[Chinf, CL, GammaHinf, info] = hinfsyn(T,nmeas,ncont,'DISPLAY','ON', 'METHOD', 'ric');
%[Chinf, CL, GammaHinf, info] = dksyn(T,nmeas,ncont);
end
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
if false
    
K = 0.1;
Ta = 50/(2*pi);
Tb = 2000/(2*pi);
idealPlant = K*(Tb*s + 1)/(Ta*s + 1);    
    
W_PERF = w_perf_yaw;

systemnames = 'idealPlant Steer_Delay B_1 A S W_S W_N W_PERF'; 
inputvar = '[unc3{4}; des_yawrate{1}; noise{4}; delta{1}]';
outputvar = '[W_S; W_PERF; unc3+S+W_N; des_yawrate]';

input_to_idealPlant = '[des_yawrate]';
input_to_Steer_Delay = '[delta]';
input_to_B_1 = '[Steer_Delay]';
input_to_S = '[B_1 + A]';
input_to_W_S = '[S]';
input_to_A = '[S]';
input_to_W_PERF = '[S(4) - idealPlant]';
input_to_W_N = '[noise]';


cleanupsysic = 'yes';
T = sysic; 

hold on
sigma(T,'g')

nmeas = 5;
ncont = 1;
[Chinf, CL, GammaHinf, info] = hinfsyn(T,nmeas,ncont,'DISPLAY','ON', 'METHOD', 'lmi');
end
